/**
 * Created by Platzhalter on 05.01.2016
 */
var irc = require('irc');

var config = {
    password: null,
    userName: 'Bot',
    realName: 'Bot',
    ident: "bot@bot",
    port: 6667,
    localAddress: null,
    debug: true,
    showErrors: true,
    autoRejoin: true,
    autoConnect: true,
    channels: [],
    retryCount: null,
    retryDelay: 2000,
    secure: false,
    selfSigned: false,
    certExpired: false,
    floodProtection: false,
    floodProtectionDelay: 1000,
    sasl: false,
    stripColors: false,
    channelPrefixes: '#',
    messageSplit: 512,
    encoding: false,
    webirc: {
        pass: '',
        ip: '',
        user: ''
    },
    commandChar: "!"
};


module.exports = exports = function (listeners, conf) {
    for (var c in conf) if (conf.hasOwnProperty(c)) config[c] = conf[c];

    if (!(conf.hasOwnProperty('server') && conf.hasOwnProperty('nickName'))) {
        console.error("The bot needs to have a server and a nickname specified!");
        process.exit(1);
    }

    var client = new irc.Client(config.server, config.nickName, config);

    if (!listeners.hasOwnProperty('DEFAULT')) listeners.DEFAULT = function (sender, channel, command) {
        client.say(channel, "The command \"" + command + "\" does not exist.");
    };

    client.connect(100, function () {
        if (config.operPassword) client.send('OPER', config.nickName, config.operPassword);

        if (config.commandChar != null)
            client.addListener('message', function (from, to, message) {
                if (to != client.nick)
                    if (message.substring(0, 1) == config.commandChar && message.length > 1) {
                        var split = message.split(config.commandChar)[1].split(' ');
                        var cmd = split[0];
                        var args = [];
                        for (var i = 1; i < split.length; i++) args[i - 1] = split[i];

                        if (listeners.command.hasOwnProperty(cmd.toUpperCase())) {
                            listeners[cmd.toUpperCase()].apply(client, [client, from, to, cmd].concat(args));
                        }
                        else if (listeners.hasOwnProperty("DEFAULT")) {
                            listeners.DEFAULT(from, to, cmd);
                        }
                        else console.error("Error: No DEFAULT function");
                    } else {
                        listeners.message(client, from, to, message);
                    }
            });
        client.addListener('pm', function (from, message) {
            listeners.pm(client, from, message, from.isOper);
        });
        client.addListener('notice', function (from, to, text, message) {
            if (to == config.nickName) listeners.notice(client, from, text, message);
        });
        client.addListener('error', function (message) {
            console.log("ERROR: " + message);
        });
    });
};